# Java EE 8 - Weather App
This directory contains an example Java EE 8 app that is used by the Java EE 8 scenarios of learn.openshift.com

It is used to demonstrate how to configure and deploy a JBoss EAP application to OpenShift.

## DataSource Configuration
The extensions folder contains files necessary to install the oracle jdbc driver module in the container.  This is used during the docker build process.

## Database Datasource Configuration
The connection information for the Oracle Datasource is configured by environment variables in the src/main/kubernetes/kent-weather-app/templates/deployment.yaml.

```
          env:
          - name: DATASOURCES
            valueFrom:
              configMapKeyRef: 
                name: database-config
                key: DATASOURCES
          - name: CESDB_DRIVER
            valueFrom:             
              configMapKeyRef: 
                name: database-config
                key: CESDB_DRIVER
          - name: CESDB_JNDI
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_JNDI
          - name: CESDB_USERNAME
            valueFrom:
              secretKeyRef:
                name: database-secret
                key: username
          - name: CESDB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: database-secret
                key: password
          - name: CESDB_JTA
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_JTA
          - name: CESDB_NONXA
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_NONXA
          - name: CESDB_URL
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_URL
          - name: CESDB_MIN_POOL_SIZE
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_MIN_POOL_SIZE
          - name: CESDB_MAX_POOL_SIZE
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_MAX_POOL_SIZE
          - name: CESDB_EXCEPTION_SORTER
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_EXCEPTION_SORTER
          - name: CESDB_BACKGROUND_VALIDATION
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_BACKGROUND_VALIDATION
          - name: CESDB_CONNECTION_CHECKER
            valueFrom: 
              configMapKeyRef: 
                name: database-config
                key: CESDB_CONNECTION_CHECKER
```

All the values come from the configMap.yaml file except the username and password.  
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: database-config
data:
  DATASOURCES: "CESDB"
  CESDB_DRIVER: "oracle"
  CESDB_JNDI: "java:jboss/datasources/CesDS"
  CESDB_JTA: "false"
  CESDB_NONXA: "true"
  CESDB_URL: "jdbc:oracle:thin:@cesrosanpdb1.ccrugylfyiad.us-east-1.rds.amazonaws.com:1521:CESNP1"
  CESDB_MIN_POOL_SIZE: "5"
  CESDB_MAX_POOL_SIZE: "10"
  CESDB_EXCEPTION_SORTER: "org.jboss.jca.adapters.jdbc.extensions.oracle.OracleExceptionSorter"
  CESDB_BACKGROUND_VALIDATION: "true"
  CESDB_CONNECTION_CHECKER: "org.jboss.jca.adapters.jdbc.extensions.oracle.OracleValidConnectionChecker"
```

The username and password are configured in a secret.  To create this secret, execute the following command.  

```
oc create secret generic database-secret --from-literal=username=<USERNAME> --from-literal=password=<PASSWORD>
```
Provide a valid value for USERNAME and PASSWORD.

## Build the war file
```
mvn clean install
```
## Build the Docker Container
```
docker build -f src/main/docker/Dockerfile -t 712731808180.dkr.ecr.us-east-1.amazonaws.com/kent-weather-app:1.0 .
```
## Push the container to ECR
```
docker login 712731808180.dkr.ecr.us-east-1.amazonaws.com --username=AWS  --password $(aws ecr get-login-password)

docker push 712731808180.dkr.ecr.us-east-1.amazonaws.com/kent-weather-app:1.0
```

## Deploy the proejct
```
oc project kent-weather-app
oc create secret generic database-secret --from-literal=username=<USERNAME> --from-literal=password=<PASSWORD>
helm install kent-weather-app src/main/kubernetes/kent-weather-app/
```
